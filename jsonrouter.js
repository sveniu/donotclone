/*
 * A routing engine.
 */

'use strict';

const jsonPath = require('jsonpath-plus');

class JsonRouter {
  constructor(config) {
    this.config_ = config;
  }

  route(payload, callback) {
    const routes = this.config_.routes;
    let targets = [];

    /* Loop over routes. */
    routeloop: for (let i = 0; i < routes.length; i++) {
      const route = routes[i];

      /* Loop over route selectors. */
      for (let j = 0; j < route.selectors.length; j++) {
        const selector = route.selectors[j];
        if (jsonPath(selector, payload).length === 0) {
          /* Selector mismatch. Skip entire route. */
          continue routeloop;
        }
      }

      /* Route match. Add target if not already present. */
      for (let j = 0; j < route.targets.length; j++) {
        const target = route.targets[j];
        if (targets.indexOf(target) == -1) {
          targets.push(target);
        }
      }
    }

    callback(payload, targets);
  }
}

module.exports = JsonRouter;
