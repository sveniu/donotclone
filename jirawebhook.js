/*
 * Set up an HTTP server and listen for inbound webhook requests from
 * JIRA. The exported JiraWebhook class accepts two args:
 *
 *   config    For example { hostname: 'localhost', port: 8080 }
 *   callback  Executed with the JIRA issue object as the only arg.
 */

'use strict';

const http = require('http');
const util = require('util');

class JiraWebhook {
  constructor(config, callback) {
    this.config_ = config;
    this.gotJiraIssueCallback_ = callback;

    const server = http.createServer(
        (request, response) => this.gotRequest_(request, response));
    server.listen(
        this.config_.port, this.config_.hostname,
        () => console.log(
            'Listening for JIRA webhooks on http://%s:%s',
            this.config_.hostname, this.config_.port));
  }

  gotRequest_(request, response) {
    let body = [];

    request.on('error', err => {
      console.error('HTTP request error: ' + err);
      response.statusCode = 400;
      response.end();
    });

    request.on('data', chunk => body.push(chunk));

    request.on('end', () => {
      let json;

      response.statusCode = 200;
      try {
        json = JSON.parse(Buffer.concat(body).toString());
      } catch (e) {
        console.error('JSON parse error: ' + e);
        response.statusCode = 400;
      }

      if (json) {
        this.gotJiraIssueCallback_(json);
      }

      response.end();
    });
  }
}

module.exports = JiraWebhook;
