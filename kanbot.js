/*
 * Kanbot, a simple Slack bot for JIRA kanban board notifications.
 */

'use strict';

const fs = require('fs');
const JiraWebhook = require('./jirawebhook');
const SlackWebhook = require('./slackwebhook');
const JsonRouter = require('./jsonrouter');
const config = require('./' + process.env.KANBOT_CONFIG_FILE);

/* Dump JSON to disk if DEBUG_JSON_DUMP is set. */
function dumpJson(json) {
  if (!('DEBUG_JSON_DUMP' in process.env)) {
    return;
  }

  const filename = config.appname + '.' + Date.now() + '.' +
      Math.random().toString(36).substring(2, 6) + '.json';
  fs.writeFileSync(filename, JSON.stringify(json, null, 4));
  console.log('JSON dumped to file \'%s\'.', filename);
}

function handleRoutingResult(payload, targets) {
  if (targets.length === 0) {
    /* No route match. */
    return;
  }

  /* Loop over targets, sending a message to each. */
  for (let i = 0; i < targets.length; i++) {
    slackWebhook.sendMessage(targets[i], payload.description);
  }
}

function handleJiraWebhookIssue(webhookData) {
  /* Put the JIRA issue in a wrapper to ease routing decisions. */
  let routeObject = {
    type: 'JIRA_WEBHOOK',
    description: 'Received JIRA webhook event',
    payload: webhookData
  };
  jsonRouter.route(routeObject, handleRoutingResult);
}

console.log('Starting app \'%s\'.', config.appname);
const jiraWebhook = new JiraWebhook(config.jirawebhook, handleJiraWebhookIssue);
const slackWebhook = new SlackWebhook(config.slackwebhook);
const jsonRouter = new JsonRouter(config.jsonrouter);
