/*
 * Kanbot configuration file example and reference.
 *
 * This file serves as an example and reference for authoring real
 * configuration files for the Kanbot service. It is usually kept up
 * to date with the capabilities of the code itself.
 */

'use strict';

/* Main configuration object. */
let config = {};
config.appname = 'kanbot';

/* Inbound JIRA webhook configuration. */
config.jirawebhook = {};
config.jirawebhook.hostname = '127.0.0.1';
config.jirawebhook.port = 8080;

/* Outbound Slack webhook configuration. */
config.slackwebhook = {};
config.slackwebhook.webhookUrl = 'https://hooks.slack.com/services/xxx';
config.slackwebhook.username = 'kanbot';
config.slackwebhook.link_names = true;

/* JSON router configuration.
 *
 * Based on the specified selectors (JSON path expressions), a JSON object is
 * routed to the desired Slack targets (#channels or @users). The selectors are
 * evaluated by the 'jsonpath-plus' npm module [1].
 *
 * The JSON path expressions are rather verbose, as there doesn't seem to be any
 * shorthand way of matching on both the property name and its value.
 *
 * Note that raw source objects, for example JIRA webhook objects, are available
 * as a payload within a wrapper object. This wrapper holds additional
 * properties to help make routing decisions:
 *
 *   {
 *     type: 'JIRA_WEBHOOK',
 *     payload: <the full JIRA webhook object>
 *   }
 *
 * [1]: JSONPath Plus
 * https://github.com/s3u/JSONPath
 *
 */
config.jsonrouter = {};
config.jsonrouter.routes = [
  {
    'description': 'Notify #mychannel about new JIRA issues',
    'selectors': [
      '$[?(@property === "type" && @ === "JIRA_WEBHOOK")]',
      '$.payload[?(@property === "webhookEvent" && @ === "jira:issue_created")]',
    ],
    'targets': [ '#mychannel' ],
  },
  {
    'description': 'Notify #mychannel about new XYZ P1 issues',
    'selectors': [
      '$[?(@property === "type" && @ === "JIRA_WEBHOOK")]',
      '$.payload[?(@property === "webhookEvent" && @ === "jira:issue_created")]',
      '$.payload.issue.fields.project[?(@property === "key" && @ === "XYZ")]',
      '$.payload.issue.fields[?(@property === "name" && @ === "P1")]',
    ],
    'targets': [ '#mychannel' ],
  },
  {
    'description': 'Notify #mychannel about escalated XYZ P1 issues',
    'selectors': [
      '$[?(@property === "type" && @ === "JIRA_WEBHOOK")]',
      '$.payload[?(@property === "webhookEvent" && @ === "jira:issue_updated")]',
      '$.payload.issue.fields.project[?(@property === "key" && @ === "XYZ")]',
      '$.payload.changelog.items[?(@.field === "priority")][?(@property === "toString" && @ === "P1")]',
    ],
    'targets': [ '#mychannel' ],
  },
];

module.exports = config;
