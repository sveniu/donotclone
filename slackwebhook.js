/*
 * Send Slack messages through an outbound webhook.
 *
 *   https://api.slack.com/incoming-webhooks
 */

'use strict';

const Slack = require('node-slack');

class SlackWebhook {
  constructor(config) {
    this.config_ = config;
    this.slack_ = new Slack(this.config_.webhookUrl);
  }

  sendMessage(target, message) {
    this.slack_.send({
      username: this.config_.username,
      link_names: this.config_.link_names ? 1 : 0,
      channel: target,
      text: message,
    });
  }
}

module.exports = SlackWebhook;
